## Agenda
- Answer questions (Async Reading)
- Address items in Verify questions issue

### Week 2 Specifics
* Merge the MRs
* Perform manual deployment
* Review the .gitlab-ci.yml contents if needed
  * Show CI Pipeline editor
* Create a release with notes

~meeting_agenda