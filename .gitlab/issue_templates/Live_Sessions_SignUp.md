| Week | Topic | Session Facilitator | Session Time | Agenda Issue Link | Async Questions Issue Link
| ------ | ------ | ------ | ------ | ------ | ----- |
| Week 1 | Create | OPEN |  | [Agenda]()
| Week 2 | Verify | OPEN |  | [Agenda]()
| Week 3 | Plan | OPEN |  | [Agenda]()
| Week 4 | Secure | OPEN |  | [Agenda]()

Expectations for Facilitators:
- Review content for the week you are leading
- Check that all participants are up to speed on reading and exercises
- Follow agenda for the week you are leading
- Answer questions in the async questions issue for the week's content
- Ensure meeting is recorded
- Update meeting agenda with recording link
- Close meeting agenda


Time commitment expectation - roughly 2 hours (prep, 30 minute session, close-out items)

~planning
